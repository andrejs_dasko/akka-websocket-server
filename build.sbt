name := "MyWebSocketServer"

version := "0.1"

scalaVersion := "2.11.8"

val akka = "com.typesafe.akka"
val akkaV = "2.5.13"
val akkaHttpV = "10.1.3"

libraryDependencies ++= Seq(
  akka %% "akka-actor"                % akkaV,
  akka %% "akka-stream"               % akkaV,
  akka %% "akka-stream-testkit"       % akkaV,
  akka %% "akka-testkit"              % akkaV % "test",
  akka %% "akka-http-core"            % akkaHttpV,
  akka %% "akka-http-testkit"         % akkaHttpV,
  akka %% "akka-http-spray-json"      % akkaHttpV,
  "org.scalatest" %% "scalatest" % "3.0.0" % "test"
//  "com.typesafe.akka" %% "akka-http-experimental" % "2.4.4"
//  "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.3"
)