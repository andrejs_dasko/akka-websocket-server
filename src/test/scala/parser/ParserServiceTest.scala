package parser

import akka.http.scaladsl.testkit.{ScalatestRouteTest, WSProbe}
import org.scalatest.{FunSuite, Matchers}

class ParserServiceTest extends FunSuite with Matchers with ScalatestRouteTest {

  test("should create empty parser.ParserService") {
    new ParserService()
  }

  test("should be able to connect to websocket") {
    val parserService = new ParserService()
    val wsClient = WSProbe()
    WS("/ws_api", wsClient.flow) ~> parserService.wsRoutes ~>
      check {
        // check response for WS Upgrade headers
        isWebSocketUpgrade shouldEqual true
      }
  }

  test("service should parse a simple json") {
    val parserService = new ParserService()
    val wsClient = WSProbe()
    WS("/ws_api", wsClient.flow) ~> parserService.wsRoutes ~>
      check {
        wsClient.sendMessage("{\"name\": \"john\"}")
        wsClient.expectMessage("Message parsed. Name attribute: john")
      }
  }
}
