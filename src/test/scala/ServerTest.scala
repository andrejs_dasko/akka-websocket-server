import ParserActor._
import UserActor.Identify
import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.http.scaladsl.model.ws.{BinaryMessage, Message, TextMessage}
import akka.http.scaladsl.server.Directives
import akka.http.scaladsl.testkit.{ScalatestRouteTest, WSProbe}
import akka.stream.{ActorMaterializer, FlowShape}
import akka.stream.scaladsl.{Flow, GraphDSL, Merge, Sink, Source}
import org.scalatest.{FunSuite, Matchers}

class ServerTest extends FunSuite with Matchers with ScalatestRouteTest {

  test("should create empty parser.ParserService") {
    new ExampleParserService()
  }

  test("should be able to connect to websocket") {
    val parserService = new ExampleParserService()
    val wsClient = WSProbe()
    WS("/ws_api", wsClient.flow) ~> parserService.websocketRoute ~>
      check {
        // check response for WS Upgrade headers
        isWebSocketUpgrade shouldEqual true
      }
  }

  test("websocket root should respond with basic greating") {
    val parserService = new ExampleParserService()
    val wsClient = WSProbe()
    WS("/ws_api", wsClient.flow) ~> parserService.websocketRoute ~>
      check {
        wsClient.sendMessage("{\"name\": \"john\"}")
        wsClient.expectMessage("Hello, john")
      }
  }

  test("websocket should list connected users") {
    val parserService = new ExampleParserService()
    val wsClient = WSProbe()
    WS("/ws_api", wsClient.flow) ~> parserService.websocketRoute ~>
      check {
        wsClient.sendMessage("{\"name\": \"john\"}")
        wsClient.expectMessage("Hello, john")
        wsClient.sendMessage("{\"name\": \"andrew\"}")
        wsClient.expectMessage("Hello, andrew")
      }

    val wsClient2 = WSProbe()
    WS("/ws_api_list_users", wsClient2.flow) ~> parserService.websocketRoute ~>
      check {
        wsClient2.sendMessage("anything")
        wsClient2.expectMessage("Users listed")
      }
  }
}

class ExampleParserService() extends Directives {
  implicit val actorSystem = ActorSystem()
  implicit val actorMaterializer = ActorMaterializer()

  val parserActor = actorSystem.actorOf(Props[ParserActor])

  val websocketRoute =
    path("ws_api") {
      handleWebSocketMessages(greetNewUser)
    }~path("ws_api_list_users") { // TODO: Unable to use path like "ws_api/list_users" for some reason
      handleWebSocketMessages(listUsersFlow)
    }

  def listUsersFlow: Flow[Message, Message, Any] =
    Flow[Message].collect {
      case TextMessage.Strict(text) => {
        parserActor ! ListUsers
        TextMessage("Users listed")
      }
    }

  def greetNewUser: Flow[Message, Message, Any] =
    Flow[Message].collect {
      case TextMessage.Strict(text) => {
        import spray.json._
        import DefaultJsonProtocol._
        implicit val userFormat = jsonFormat1(User)

        val user = text.parseJson.convertTo[User]
        parserActor ! UserJoinRequest(user)
        TextMessage(s"Hello, ${user.name}")
      }
    }

  def customGraphFlow: Flow[Message, Message, Any] =
    Flow.fromGraph(GraphDSL.create() { implicit builder =>
    import GraphDSL.Implicits._

    val materialization = builder.materializedValue.map(m => TextMessage("Welcome, user!"))
    val messagePassingFlow = builder.add(Flow[Message].map(m => m))
    val merge = builder.add(Merge[Message](2))

    materialization ~> merge.in(0)
    merge -> messagePassingFlow

    FlowShape(merge.in(1), messagePassingFlow.out)
  })
}


object ParserActor {
  sealed trait ParserRequest
  case class UserJoinRequest(user: User) extends ParserRequest
  case object UserQuitRequest extends ParserRequest
  case object ListUsers extends ParserRequest

  sealed trait ParserResponse
  case class ListUsersResponse(users: Set[User]) extends ParserResponse
  case class UserCreated(user: User) extends ParserResponse
  case class UserExists(user: User) extends ParserResponse
}
class ParserActor extends Actor {
  var users = Map.empty[String, ActorRef]

  override def receive: Receive = {
    case UserJoinRequest(user) => {
      users.get(user.name) match {
        case Some(actorRef) => {
          println(s"Actor for ${user.name} already exists")
          sender() ! UserExists(user)
        }
        case None => {
          val userActor = context.actorOf(UserActor.props(user), s"user-${user.name}")
          context.watch(userActor)
          users += user.name -> userActor
          userActor ! Identify
          println(s"Registered user with name ${user.name}")
          sender() ! UserCreated(user)
        }
      }
    }
    case UserQuitRequest => ???
    case ListUsers => {
      println("Printing user list")
      users.values.foreach(println)
    }
  }
}

case class User(name: String)

object UserActor {
  def props(user: User) = Props(new UserActor(user))

  case object Identify
}

class UserActor(user: User) extends Actor {
  override def receive: Receive = {
    case Identify => s"Hi, my name is ${user.name}"
  }
}
