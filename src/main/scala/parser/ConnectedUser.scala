package parser

import akka.actor.{Actor, ActorRef, Props}

object ConnectedUser {
  def props(parserActor: ActorRef) = Props(new ConnectedUser(parserActor))

  sealed trait UserMessage
  case class Connected(outgoingActor: ActorRef)
  case class IncomingMessage(text: String) extends UserMessage
  case class OutgoingMessage(text: String) extends UserMessage
}
class ConnectedUser(parserActor: ActorRef) extends Actor {
  import ConnectedUser._

  override def receive: Receive = disconnected

  def disconnected: Receive = {
    case Connected(wsUser) => {
      context become connected(wsUser)
      parserActor ! ParserActor.Join
    }
  }

  def connected(wsUser: ActorRef): Receive = {
    case ConnectedUser.IncomingMessage(text) => {
      // parse json message into model class
      // do necessary conversions
      // return to wsClient

      import spray.json._
      import DefaultJsonProtocol._
      implicit val userFormat = jsonFormat1(JsonRequest)
      val jsonRequest = text.parseJson.convertTo[JsonRequest]

      wsUser ! ConnectedUser.OutgoingMessage(s"Message parsed. Name attribute: ${jsonRequest.name}")
    }
  }
}

case class JsonRequest(name: String)
