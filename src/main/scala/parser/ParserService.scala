package parser

import akka.NotUsed
import akka.actor.{ActorSystem, PoisonPill, Props}
import akka.http.scaladsl.model.ws.{Message, TextMessage}
import akka.http.scaladsl.server.Directives
import akka.stream.scaladsl.{Flow, Sink, Source}
import akka.stream.{ActorMaterializer, OverflowStrategy}

class ParserService(implicit val actorSystem: ActorSystem, implicit  val actorMaterializer: ActorMaterializer) extends Directives {

  def wsRoutes = path("ws_api") {
    handleWebSocketMessages(newUserFlow)
  }

  val parserActor = actorSystem.actorOf(Props[ParserActor])

  def newUserFlow: Flow[Message, Message, NotUsed] = {
    val connectedWsActor = actorSystem.actorOf(ConnectedUser.props(parserActor))

    val incomingMessages: Sink[Message, NotUsed] =
      Flow[Message].map {
        case TextMessage.Strict(text) => ConnectedUser.IncomingMessage(text)
      }.to(Sink.actorRef(connectedWsActor, PoisonPill))

    val outgoingMessages: Source[Message, NotUsed] =
      Source
        .actorRef[ConnectedUser.OutgoingMessage](10, OverflowStrategy.fail)
        .mapMaterializedValue { outgoingActor =>
          connectedWsActor ! ConnectedUser.Connected(outgoingActor)
          NotUsed
        }
        .map {
          case ConnectedUser.OutgoingMessage(text) => TextMessage(text)
        }

    Flow.fromSinkAndSource(incomingMessages, outgoingMessages)
  }
}