package parser

import akka.actor.{Actor, ActorRef, Terminated}
import parser.ParserActor.Join

object ParserActor {

  case object Join
}
class ParserActor extends Actor {
  var users: Set[ActorRef] = Set.empty

  override def receive: Receive = {
    case Join => {
      users += sender()
      context.watch(sender())
    }
    case Terminated(user) => {
      println(s"User disconnected: $user")
      users -= user
    }
  }
}