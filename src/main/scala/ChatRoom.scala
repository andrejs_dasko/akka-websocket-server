import ChatRoom.Join
import akka.actor.{Actor, ActorRef, Props, Terminated}

object ChatRoom {
  def props: Props = Props(new ChatRoom)

  case object Join
  case class ChatMessage(message: String)
}

class ChatRoom extends Actor {
  var users: Set[ActorRef] = Set.empty

  override def receive: Receive = {
    case Join => {
      users += sender()
      context.watch(sender())
    }
    case msg => {
      users.foreach(_ ! msg)
    }
    case Terminated(user) => users -= user
  }

}
