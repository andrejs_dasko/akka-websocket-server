import akka.actor.ActorSystem
import akka.http.scaladsl.model.ws.{BinaryMessage, Message, TextMessage}
import akka.http.scaladsl.server.Directives.{complete, get, handleWebSocketMessages, parameter, parameterMap, path, _}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Sink, Source}
import spray.json.{DefaultJsonProtocol, _}

trait Routes {
  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()

  def wsRoutes =
    path("echo") {
      get {
        handleWebSocketMessages(echo)
      }
    }~path("parse") {
      handleWebSocketMessages(parse)
    }~path("greeter") {
      handleWebSocketMessages(greeter)
    }~path("auth") {
      parameter('user) { user =>
        handleWebSocketMessages(auth(user))
      }
    }~path("login") {
      parameterMap { map =>
        handleWebSocketMessages(login(map))
      }
    }

  def httpRoutes =
    path("example") {
      get {
        complete("Such response, much wow!")
      }
    }

  val echo = Flow[Message]

  final case class Vegetable(name: String, colour: String)

  object VegetableFormat extends DefaultJsonProtocol {
    implicit val vegetableFormat = jsonFormat2(Vegetable)
  }

  def parse: Flow[Message, Message, Any] =
    Flow[Message].collect {
      case TextMessage.Strict(text) => {
        import VegetableFormat._
        val json = text.parseJson
        val vegetable = json.convertTo[Vegetable]
        val responseMessage = s"Vegetable successfully unmarshalled. Name: ${vegetable.name}, colour: ${vegetable.colour}"
        TextMessage(Source.single(responseMessage))
      }
    }

  def greeter: Flow[Message, Message, Any] =
    Flow[Message].mapConcat {
      case tm: TextMessage =>
        TextMessage(Source.single("Hello, ") ++ tm.textStream ++ Source.single("!")) :: Nil
      case bm: BinaryMessage =>
        bm.dataStream.runWith(Sink.ignore)
        Nil
    }

  def auth(user: String): Flow[Message, Message, Any] = {
    Flow[Message].mapConcat {
      case tm: TextMessage => {
        println(user)
        TextMessage(Source.single("Hello")) :: Nil
      }
      case bm: BinaryMessage =>
        bm.dataStream.runWith(Sink.ignore)
        Nil
    }
  }

  def login(parameters: Map[String, String]): Flow[Message, Message, Any] = {
    Flow[Message].mapConcat {
      case tm: TextMessage => {
        if (credentialsValid(parameters)) {
          println("Valid credentials")
          TextMessage(Source.single("Welcome, user!")) :: Nil
        } else {
          println("Invalid credentials")
          TextMessage(Source.single("Incorrect user/pass")) :: Nil
        }
      }

      case bm: BinaryMessage =>
        bm.dataStream.runWith(Sink.ignore)
        Nil
    }
  }

  def credentialsValid(parameters: Map[String, String]): Boolean = {
    parameters.get("user") match {
      case Some("admin") => parameters.get("pass") match {
        case Some("admin") => true
        case _ => false
      }
      case _ => false
    }
  }
}

