import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.server.Directives
import spray.json.DefaultJsonProtocol

// collect your json format instances into a support trait:
trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val vegetableFormat = jsonFormat2(Vegetable)
}

// domain model
final case class Vegetable(name: String, colour: String)

// use it wherever json (un)marshalling is needed
trait MyJsonService extends Directives with JsonSupport {

  // format: OFF
  def jsonRoutes =
    get {
      pathSingleSlash {
        complete(Vegetable("tomato", "red")) // will render as JSON
      }
    }~
    post {
      pathSingleSlash {
        entity(as[Vegetable]) { vegetable => // will unmarshal JSON to Order
          val name = vegetable.name
          val colour = vegetable.colour
          complete(s"Vegetable: $name colour: $colour")
        }
      }
    }
  // format: ON
}