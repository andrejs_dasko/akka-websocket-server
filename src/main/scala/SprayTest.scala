import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol
import spray.json._

class SprayTest extends SprayJsonSupport with DefaultJsonProtocol {
  final case class Vegetable(name: String, colour: String)

  def main(args: Array[String]): Unit = {
    val vegetableFormat = jsonFormat2(Vegetable)
    val json = "{\"name\": \"tomato\",\"colour\": \"red\"}".stripMargin.parseJson
    val vegetable = vegetableFormat.read(json)
    println(s"Vegetable: ${vegetable.name} colour: ${vegetable.colour}")
  }
}
