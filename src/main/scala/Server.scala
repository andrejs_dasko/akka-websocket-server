import akka.actor.ActorSystem
import akka.http.scaladsl._
import akka.stream.ActorMaterializer
import parser.ParserService

import scala.io.StdIn

object Server {

  object Config {
    val host = "127.0.0.1"
    val port = 9000
  }

  def main(args: Array[String]) {
    implicit val system = ActorSystem()
    implicit val materializer = ActorMaterializer()
    implicit val executionContext = system.dispatcher

    val parserService = new ParserService()
    val bindingFuture = Http().bindAndHandle(parserService.wsRoutes, Config.host,Config.port)
    println(s"Server online at ${Config.host}:${Config.port}\nPress RETURN to stop...")
    StdIn.readLine()
    bindingFuture
      .flatMap(_.unbind())
      .onComplete(_ => system.terminate())
  }
}

